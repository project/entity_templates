<?php


namespace Drupal\entity_templates\EventSubscriber;

use Drupal\entity_clone\Event\EntityCloneEvent;
use Drupal\entity_clone\Event\EntityCloneEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;


class EntityTemplatesEventSubscriber implements EventSubscriberInterface {
  /**
   * {@inheritdoc}
   *
   * @return array
   *   The event names to listen for, and the methods that should be executed.
   */
  public static function getSubscribedEvents() {
    return [
      EntityCloneEvents::PRE_CLONE => 'preClone',
    ];
  }

  /**
   * @param \Drupal\entity_clone\Event\EntityCloneEvent $event
   */
  public function preClone(EntityCloneEvent $event) {
    $cloned_entity = $event->getClonedEntity();
    if ($cloned_entity->entity_is_template->value) {
      // Ensures the cloned entity is not also marked as a template.
      $cloned_entity->entity_is_template = FALSE;
    }
  }
}
